import axios from "axios";

const API_KEY = "a4b87160a3171ba343b63c5896dc737b";
const TOKEN = "acf08d8e3bf2e2ae3516012bab0b4c6f692cdb5133109a6787c5e3fb884dbb4c"

export function getBoards() {
    return axios.get(`https://api.trello.com/1/members/me/boards?fields=name,url&key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function createBoard(title) {
    return axios.post(`https://api.trello.com/1/boards/?name=${title}&key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function getLists(id) {
    return axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function createLists(id, title) {
    return axios.post(`https://api.trello.com/1/boards/${id}/lists?name=${title}&key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function deleteList(id) {
    return axios.put(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${API_KEY}&token=${TOKEN}`)

}

export function getCards(id) {
    return axios.get(`https://api.trello.com/1/lists/${id}/cards?key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}
export function createCard(id, title) {
    return axios.post(`https://api.trello.com/1/cards?name=${title}&idList=${id}&key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function deleteCard(id) {
    return axios.delete(`https://api.trello.com/1/cards/${id}?key=${API_KEY}&token=${TOKEN}`)
}

export function getCheckList(id) {
    return axios.get(`https://api.trello.com/1/cards/${id}/checklists?key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function createCheckList(id, title) {
    return axios.post(`https://api.trello.com/1/checklists?name=${title}&idCard=${id}&key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function deleteCheckList(id) {
    return axios.delete(`https://api.trello.com/1/checklists/${id}?key=${API_KEY}&token=${TOKEN}`)
}

export function getCheckItem(id) {
    return axios.get(`https://api.trello.com/1/checklists/${id}/checkItems?key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function createCheckItem(id, title) {
    return axios.post(`https://api.trello.com/1/checklists/${id}/checkItems?name=${title}&key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}

export function deleteCheckItem(listId, itemId) {
    return axios.delete(`https://api.trello.com/1/checklists/${listId}/checkItems/${itemId}?key=${API_KEY}&token=${TOKEN}`)
}

export function updateCheckItem(cardId, itemId, status) {
    return axios.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${status}&key=${API_KEY}&token=${TOKEN}`)
        .then(res => res.data)
}
