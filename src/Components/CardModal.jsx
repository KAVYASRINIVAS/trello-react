import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import { getCheckList, deleteCheckList, createCheckList } from "../api";
import CheckList from "./CheckList";

class CardModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkListData: [],
      checklistTitle: "",
    };
  }

  componentDidMount() {
    const id = this.props.cardInfo.id;
    getCheckList(id).then((data) => {
      this.setState({
        checkListData: data,
      });
    });
  }

  handleTitle = (e) => {
    this.setState({
      checklistTitle: e.target.value,
    });
  };

  handleCreateCheckList = (id, title) => {
    if (title !== "") {
      createCheckList(id, title).then((data) => {
        this.setState({
          checkListData: [...this.state.checkListData, data],
          checklistTitle: "",
        });
      });
    }
  };
  handleDeleteCheckList = (id) => {
    deleteCheckList(id).then((res) => {
      console.log("CheckList deleted!");
      this.setState({
        checkListData: this.state.checkListData.filter(
          (checklist) => checklist.id !== id
        ),
      });
    });
  };

  render() {
    console.log(this.props.cardInfo);
    return (
      <Modal
        show={this.props.modalStatus}
        onHide={this.props.modalStatus}
        style={{
          borderRadius: "10px",
          fontFamily: "'Roboto Slab', serif",
        }}
      >
        <div className="d-flex flex-column m-3">
          <div className="d-flex">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="25"
              height="25"
              fill="currentColor"
              classBox="bi bi-card-heading "
              viewBox="0 0 16 16"
            >
              <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z" />
              <path d="M3 8.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0-5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-1z" />
            </svg>
            <p className="ModalTitle">{this.props.cardInfo.name}</p>
          </div>
          <p className="mx-3 ModalSubTitle">
            in the {this.props.listInfo.name}
          </p>
        </div>

        <section className="newCheckList">
          <form
            className="d-flex flex-column p-3 m-3 justify-content-between rounded modalForm "
            onSubmit={(e) => {
              e.preventDefault();
              this.handleCreateCheckList(
                this.props.cardInfo.id,
                this.state.checklistTitle
              );
            }}
          >
            <input
              type="text"
              autoFocus
              placeholder="Enter checklist title"
              value={this.state.checklistTitle}
              onChange={this.handleTitle}
            />
            <div className="m-2 d-flex align-items-center fw-bold">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-plus-circle text-white "
                viewBox="0 0 16 16"
              >
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
              </svg>{" "}
              <button className="border-0 bg-transparent text-white fw-bold">
                Create
              </button>
            </div>
          </form>
        </section>

        <div>
          {this.state.checkListData.map((checkList) => (
            <CheckList
              key={checkList.id}
              cardId={this.props.cardInfo.id}
              checkListData={checkList}
              deleteCheckList={this.handleDeleteCheckList}
            />
          ))}
        </div>
      </Modal>
    );
  }
}

export default CardModal;
