import React, { Component } from "react";
import { updateCheckItem } from "../api";

class CheckItem extends Component {
  handleItem = (cardID, itemID, status) => {
    updateCheckItem(cardID, itemID, status).then((data) =>
      this.props.updateItem(data)
    );
  };

  render() {
    // console.log(this.props.checkItemData);
    return (
      <section className="CheckItem">
        <div className="d-flex m-2 justify-content-between">
          <div>
            <input
              type="checkbox"
              id={this.props.checkItemData.id}
              onChange={(e) =>
                this.handleItem(
                  this.props.cardID,
                  this.props.checkItemData.id,
                  e.target.checked
                )
              }
              checked={
                this.props.checkItemData.state === "complete" ? true : false
              }
            />{" "}
            <label htmlFor={this.props.checkItemData.name}>
              {this.props.checkItemData.name}
            </label>
          </div>
          <button
            className="border-0"
            onClick={() =>
              this.props.deleteCheckItem(
                this.props.listID,
                this.props.checkItemData.id
              )
            }
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              fill="currentColor"
              class="bi bi-x-circle"
              viewBox="0 0 16 16"
            >
              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
              <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
            </svg>
          </button>
        </div>
      </section>
    );
  }
}

export default CheckItem;
