import React, { Component } from "react";
import { getBoards, createBoard } from "../api";
import BoardsList from "./BoardsList";
import PopUp from "./PopUp";

class BoardContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boards: [],
      show: false,
      spinner: true,
    };
  }

  componentDidMount() {
    getBoards().then((boards) => {
      this.setState({ boards, spinner: false });
    });
  }

  handleModal = () => {
    this.setState({
      show: !this.state.show,
    });
  };

  handleTitle = (newTitle) => {
    createBoard(newTitle).then((data) => {
      console.log(data);
      this.setState({
        boards: [...this.state.boards, data],
      });
    });
  };

  render() {
    return (
      <div className="d-flex flex-column">
        {this.state.show === true ? (
          <PopUp
            toggleShow={this.handleModal}
            handleBoardTitle={this.handleTitle}
          />
        ) : (
          ""
        )}
        <BoardsList
          boardData={this.state.boards}
          toggleShow={this.handleModal}
          showBoard={this.handleShowBoard}
          spinner={this.state.spinner}
        />
      </div>
    );
  }
}

export default BoardContainer;
