import React, { Component } from "react";
import CardModal from "./CardModal";

class EachCard extends Component {
  constructor(props) {
    super(props);

    this.state = { modal: false, handleState: false };
  }

  handleModal = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };

  handleListState = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };

  render() {
    return (
      <>
        <section
          className="d-flex flex-column justify-content-between"
          key={this.props.card.id}
        >
          <div
            className="rounded d-flex justify-content-between px-2 py-1 my-1 Card"
            key={this.props.card.id}
            id={this.props.card.id}
          >
            <div className="CardLabel"></div>
            <span className="CardTitle" onClick={this.handleModal}>
              {" "}
              <span className="mb-1 CardLabel">{this.props.card.name}</span>
            </span>

            <button
              className="deleteBtn"
              title="Delete Card"
              onClick={() => this.props.handleDelete(this.props.card.id)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-x-circle"
                viewBox="0 0 16 16"
              >
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
              </svg>
            </button>
          </div>
        </section>
        {this.state.modal === true ? (
          <CardModal
            modalStatus={this.handleModal}
            cardInfo={this.props.card}
            listInfo={this.props.listInfo}
          />
        ) : (
          ""
        )}
      </>
    );
  }
}

export default EachCard;
